#!/bin/bash
export GPU_MAX_ALLOC_PERCENT=100
export GPU_USE_SYNC_OBJECTS=1
export GPU_FORCE_64BIT_PTR=0
export GPU_MAX_HEAP_SIZE=100
export GPU_SINGLE_ALLOC_PERCENT=100
screen -dmS miner ./sgminer -k lyra2re2 -c lyrabar.conf --auto-fan --no-submit-stale
sleep 5
screen -r miner
